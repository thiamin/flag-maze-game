# Flag Maze Game
This is a small weekend project I wrote for a friend. It's a little maze game where you have to get from the upper-left corner of the maze to the lower-right corner of the maze by clicking on neighboring countries flags. The source isn't quite polished up yet, but it will be commited soon. You can play it [here](https://flags.jordans-black-site.ml/game/index.html?id=1)

# Credit
Inspired by the [sporcle flag border maze](https://www.sporcle.com/games/broxx/flag-maze-1).
